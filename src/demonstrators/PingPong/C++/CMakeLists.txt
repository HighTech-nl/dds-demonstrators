cmake_minimum_required(VERSION 3.5)

# Find the CycloneDDS package. If it is not in a default location, try
# finding it relative to the example where it most likely resides.
find_package(CycloneDDS REQUIRED COMPONENTS idlc PATHS "${CMAKE_CURRENT_SOURCE_DIR}/../../../..")

# This is a convenience function, provided by the CycloneDDS package,
# that will supply a library target related the the given idl file.
# In short, it takes the idl file, generates the source files with
# the proper data types and compiles them into a library.
idlc_generate(MSGData_lib "MSGData.idl")

add_executable(PingPong main.cpp src/ping_pong.cpp)

# Both executables need to be linked to the idl data type library and
# the ddsc API library.
target_link_libraries(PingPong MSGData_lib CycloneDDS::ddsc)

