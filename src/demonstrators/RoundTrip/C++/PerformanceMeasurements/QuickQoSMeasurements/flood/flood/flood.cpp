#include <chrono>
#include <string>
#include <thread>

#include <iostream>

#include "flood.hpp"

/**
 * @brief Construct a new flood object
 *
 * @param id the ID of the device
 * @param totalDevices the total number of devices in the flood loop
 * @param floodMessages the number of messages that will be send / received
 */
Flood::Flood(const unsigned int id, const unsigned int totalDevices, const unsigned int floodMessages)
    : _id{id}, _totalDevices{totalDevices}, _totalFloodMsg{floodMessages}, _running{false}, _floodSend{0},
      _floodReceived{0} {}

/**
 * @brief Destroy the Flood object including allocated memory
 *
 */
Flood::~Flood() {
    dds_delete(_participant);
    PerformanceData_Msg_free(_messageMemory[0], DDS_FREE_ALL);
}

/**
 * @brief initialise initialises the flood and the correct settings for DDS
 *
 */
void Flood::initialise(QoSManager *qos) {
    _participant = dds_create_participant(DDS_DOMAIN_DEFAULT, NULL, NULL);
    std::string readTopic = "flood" + std::to_string(_id);
    std::string writeTopic = "flood" + std::to_string(_id + 1);
    if (_id == _totalDevices) {
        writeTopic = "flood1";
    }

    _topicWrite =
        dds_create_topic(_participant, &PerformanceData_Msg_desc, writeTopic.c_str(), qos->getWriteTopic(), NULL);
    _topicRead =
        dds_create_topic(_participant, &PerformanceData_Msg_desc, readTopic.c_str(), qos->getReadTopic(), NULL);
    _writer = dds_create_writer(_participant, _topicWrite, qos->getWriter(), NULL);
    _reader = dds_create_reader(_participant, _topicRead, qos->getReader(), NULL);

    _msg.key = static_cast<int32_t>(_id);

    _messageMemory[0] = PerformanceData_Msg__alloc();
    // Sleep for initialisation
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
}

/**
 * @brief run runs the flood
 *
 * @note this function could wait infinitely!
 */
void Flood::run() {
    _running = true;
    _timer.start();
    if (_id == 1) {
        runMaster();
    } else {
        runSlave();
    }
    _timer.stop();
}

/**
 * @brief runMaster runs the flood loop of the master
 *
 */
void Flood::runMaster() {
    dds_return_t readCheck;
    dds_sample_info_t infos[1];
    // Initiate the flood
    while (_running) {
        readCheck = dds_take(_reader, _messageMemory, infos, 1, 1);
        if ((readCheck > 0) && (infos[0].valid_data)) {
            if (_totalFloodMsg == ++_floodReceived) {
                _running = false;
            }
        } else {
            if (_totalFloodMsg != _floodSend) {
                _floodSend++;
                _msg.key = static_cast<int>(_floodSend);
                dds_write(_writer, &_msg);
            }
        }
    }
}

/**
 * @brief runSlave runs the flood loop of the slave
 *
 */
void Flood::runSlave() {
    dds_return_t readCheck;
    dds_sample_info_t infos[1];
    // Initiate the flood
    while (_running) {
        readCheck = dds_take(_reader, _messageMemory, infos, 1, 1);
        if ((readCheck > 0) && (infos[0].valid_data)) {
            _floodReceived++;
            _msg.key = static_cast<int>(_floodReceived);
            dds_write(_writer, &_msg);
            if (_totalFloodMsg == _floodReceived) {
                _running = false;
            }
        }
    }
}