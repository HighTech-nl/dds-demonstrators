#ifndef PERFORMANCE_TESTS_H
#define PERFORMANCE_TESTS_H

#include "flood/flood.hpp"
#include "measurements_file/measurements_file.hpp"
#include "qos_manager/qos_manager.hpp"
#include <string>

/**
 * @brief Contains a lot of different DDS performance tests that can be executed on the flood
 * It also stores the results in a file (each result will be appended to the file)
 *
 */
class PerformanceTests {
  public:
    PerformanceTests(const unsigned int id, const unsigned int totalDevices, const unsigned int floodMsg,
                     std::string filename = "measurements.csv");
    ~PerformanceTests() = default;

    void start();

  private:
    const unsigned int _id;
    const unsigned int _totalFloodMessages;
    const unsigned int _totalDevices;

    MeasurementsFile _measurementsFile;
    QoSManager _qosConfig;

    void globalTests();
    void qosPolicyTests();
    void executeTest(const std::string &testName, QoSManager *qosConfig);
};

#endif // PERFORMANCE_TESTS_H