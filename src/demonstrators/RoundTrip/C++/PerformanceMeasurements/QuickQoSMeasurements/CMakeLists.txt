cmake_minimum_required(VERSION 3.5)

# Choose the wanted configuration
## Config parameter: can be configured for building the wanted performance test
## This can be "roundtrip" or "flood"
set(PROJECT roundtrip)

# Choose the wanted project name / executable name
## This parameter changes the executable name
set(PROJECT_NAME performance_measurements_${PROJECT})

# Define the project as a CPP project
project(${PROJECT_NAME})

#[[
    Add the files to the project
]]
# Relative include path
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/
                     ${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT}
                     ${CMAKE_CURRENT_SOURCE_DIR}/../../../../CommonCode/C++/)

set(MAIN ${CMAKE_CURRENT_SOURCE_DIR}/main.cpp)

# Set the main
set(SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/../../../../CommonCode/C++/performance/performance_measurements.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT}/${PROJECT}/${PROJECT}.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT}/performance_tests/performance_tests.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/measurements_file/measurements_file.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/qos_manager/qos_manager.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/configurations/configurations.cpp)

# Add the sources and main to the executable
add_executable(${PROJECT_NAME} ${MAIN} ${SOURCES})

set_property(TARGET ${PROJECT_NAME} PROPERTY CXX_STANDARD 17)

# Add warnings
target_compile_options(${PROJECT_NAME} PRIVATE -pedantic -Wall -Wcast-align -Wcast-qual -Wdisabled-optimization 
                                               -Winit-self -Wmissing-declarations -Wmissing-include-dirs -Wredundant-decls 
                                               -Wshadow -Wsign-conversion -Wundef -Werror
                                               -Wempty-body -Wignored-qualifiers -Wmissing-field-initializers 
                                               -Wsign-compare -Wtype-limits  -Wuninitialized )
#[[
    Installed libraries for this project
]]
### Threading library
find_package(Threads REQUIRED)

# Add the threading library to the project
set(DEPENDENCIES ${CMAKE_THREAD_LIBS_INIT})

### DDS Library (CycloneDDS)
find_package(CycloneDDS REQUIRED COMPONENTS idlc PATHS "${CMAKE_CURRENT_SOURCE_DIR}/../../../..")

# This is a convenience function, provided by the CycloneDDS package,
# that will supply a library target related the the given idl file.
# In short, it takes the idl file, generates the source files with
# the proper data types and compiles them into a library.
idlc_generate(IDLData_lib "PerformanceData.idl")


# Add the DDS library to the project
target_link_libraries(${PROJECT_NAME} IDLData_lib CycloneDDS::ddsc)

#[[
    Linking the libraries
]]
# Link libraries
target_link_libraries(${PROJECT_NAME} ${DEPENDENCIES})