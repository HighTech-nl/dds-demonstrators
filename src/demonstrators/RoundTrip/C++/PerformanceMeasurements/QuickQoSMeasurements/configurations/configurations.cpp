#include "configurations.h"

#include <iostream>

/**
 * @brief deadlineCallback is the callback for the deadline
 */
void Configurations::deadlineCallback(dds_entity_t reader, const dds_requested_deadline_missed_status_t status,
                                      void *arg) {
    std::cout << "deadline callback executed!";
}

/**
 * @brief deadline contains the deadline policy
 *
 * @param maxDeadline the maximum deadline
 */
void Configurations::deadline(dds_qos_t *qos, dds_listener_t *listener, const dds_time_t maxDeadline) {
    dds_qset_deadline(qos, maxDeadline);
    dds_lset_requested_deadline_missed(listener, deadlineCallback);
}

/**
 * @brief destination_order
 *
 * @param configuration can be DDS_DESTINATIONORDER_BY_RECEPTION or DDS_DESTINATIONORDER_BY_SOURCE_TIMESTAMP
 */
void Configurations::destination_order(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration) {
    // Destination order can be DDS_DESTINATIONORDER_BY_RECEPTION_TIMESTAMP or DDS_DESTINATIONORDER_BY_SOURCE_TIMESTAMP
    dds_qset_destination_order(qos, static_cast<dds_destination_order_kind_t>(configuration));
}

/**
 * @brief durability contains the lifespan policy
 *
 * @param configuration can be  DDS_DURABILITY_PERSISTENT, DDS_DURABILITY_TRANSIENT,
 * DDS_DURABILITY_TRANSIENT_LOCAL or DDS_DURABILITY_VOLATILE
 */
void Configurations::durability(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration) {
    // Durability can be DDS_DURABILITY_PERSISTENT, DDS_DURABILITY_TRANSIENT,
    // DDS_DURABILITY_TRANSIENT_LOCAL or DDS_DURABILITY_VOLATILE
    dds_qset_durability(qos, static_cast<dds_durability_kind_t>(configuration));
}

/**
 * @brief lifespan contains the lifespan policy
 *
 * @param maxLifespan the time of the lifespan of a message
 */
void Configurations::lifespan(dds_qos_t *qos, dds_listener_t *listener, const dds_time_t maxLifespan) {
    // Time of the lifespan can be altered
    dds_qset_lifespan(qos, maxLifespan);
}

/**
 * @brief livelinessCallback contains the callback for a changed liveliness
 */
void Configurations::livelinessCallback(dds_entity_t reader, const dds_liveliness_changed_status_t status, void *arg) {
    std::cout << "C";
}

/**
 * @brief liveliness contains the liveliness policy
 *
 * @param configuration can be DDS_LIVELINESS_MANUAL_BY_PARTICIPANT, DDS_LIVELINESS_MANUAL_BY_TOPIC or
 * DDS_LIVELINESS_AUTOMATIC
 */
void Configurations::liveliness(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration) {
    // Liveliness can be DDS_LIVELINESS_MANUAL_BY_PARTICIPANT, DDS_LIVELINESS_MANUAL_BY_TOPIC or
    // DDS_LIVELINESS_AUTOMATIC
    dds_qset_liveliness(qos, static_cast<dds_liveliness_kind_t>(configuration), DDS_MSECS(10));
    dds_lset_liveliness_changed(listener, livelinessCallback);
}

/**
 * @brief ownership contains the ownership policy
 *
 * @param configuration can be DDS_OWNERSHIP_SHARED or DDS_OWNERSHIP_EXCLUSIVE
 * @note the ownership should only apply to topics with multiple writers
 * This test mainly tests if one writer with a configured ownership also has overhead.
 */
void Configurations::ownership(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration) {
    // Ownership can be DDS_OWNERSHIP_SHARED or DDS_OWNERSHIP_EXCLUSIVE
    dds_qset_ownership(qos, static_cast<dds_ownership_kind_t>(configuration));
    dds_qset_ownership_strength(qos, 5);
}

/**
 * @brief reader_data_lifecycle contains the reader_data_lifecycle policy
 *
 * @param configuration configures the nowriter and disposed sample delay
 */
void Configurations::reader_data_lifecycle(dds_qos_t *qos, dds_listener_t *listener, const dds_time_t configuration) {
    dds_qset_reader_data_lifecycle(qos, configuration, configuration);
}

/**
 * @brief durability_service configures external persistence service for writers with PERSISTENT or TRANSIENT durability
 *
 * @param configuration the service cleanup delay
 */
void Configurations::durability_service(dds_qos_t *qos, dds_listener_t *listener, const dds_time_t configuration) {
    // History can be DDS_HISTORY_KEEP_LAST or DDS_HISTORY_KEEP_ALL
    dds_qset_durability_service(qos, configuration, DDS_HISTORY_KEEP_ALL, 5, 25, 25, 25);
}

/**
 * @brief controls the history of the messages
 *
 * @param configuration can be DDS_HISTORY_KEEP_LAST, DDS_HISTORY_KEEP_ALL
 */
void Configurations::history(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration) {
    // The static 5 is only active with DDS_HISTORY_KEEP_LAST, this is how many are kept
    dds_qset_history(qos, static_cast<dds_history_kind_t>(configuration), 1);
}

/**
 * @brief reliability contains the reliability policy
 *
 * @param configuration can be DDS_RELIABILITY_BEST_EFFORT or DDS_RELIABILITY_RELIABLE
 */
void Configurations::reliability(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration) {
    // DDS_RELIABILITY_BEST_EFFORT or DDS_RELIABILITY_RELIABLE
    dds_qset_reliability(qos, static_cast<dds_reliability_kind_t>(configuration), DDS_SECS(10));
}

/**
 * @brief latency budget is probably not the same as the the RTI latency budget.
 * This is because this function uses time and RTI uses a priority
 *
 * @param configuration the latency budget time
 */
void Configurations::latency_budget(dds_qos_t *qos, dds_listener_t *listener, const dds_time_t configuration) {
    dds_qset_latency_budget(qos, configuration);
}

/**
 * @brief presentation controls how the data is received by the reader
 *
 * @param configuration can be DDS_PRESENTATION_TOPIC, DDS_PRESENTATION_GROUP or DDS_PRESENTATION_INSTANCE
 */
void Configurations::presentation(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration) {
    // DDS_PRESENTATION_TOPIC, DDS_PRESENTATION_GROUP or DDS_PRESENTATION_INSTANCE
    dds_qset_presentation(qos, static_cast<dds_presentation_access_scope_kind_t>(configuration), true, true);
}

/**
 * @brief resource_limits controls the limits for the resources
 *
 * @param configuration the maximum amount of samples
 */
void Configurations::resource_limits(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration) {
    dds_qset_resource_limits(qos, configuration, 5, 5);
}

/**
 * @brief time_based_filter sets a minimum time period before new data is provided to a reader
 * Excess data is not send or discarded

 * @param configuration the minimum time of a new message
 */
void Configurations::time_based_filter(dds_qos_t *qos, dds_listener_t *listener, const dds_time_t configuration) {
    dds_qset_time_based_filter(qos, configuration);
}

/**
 * @brief transport_priority tells Connext that the data that is send has a different priority.
 * This QoS is only used by the underlying transport protocol if they can use it
 *
 * @param configuration the priority
 */
void Configurations::transport_priority(dds_qos_t *qos, dds_listener_t *listener, const int64_t priority) {
    dds_qset_transport_priority(qos, priority);
}

/**
 * @brief writer_data_lifecycle controls how a writer handles the lifecycle
 *
 * @param configuration can be true or false (1 or 0), this indicates whether the writer should delete an instance
 * when it unregisters (default is 1/true)
 */
void Configurations::writer_data_lifecycle(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration) {
    dds_qset_writer_data_lifecycle(qos, configuration);
}

/**
 * @brief partition adds additional identifiers for multiple writers/readers on a topic
 *
 * @param configuration this configuration parameter is not used
 */
void Configurations::partition(dds_qos_t *qos, dds_listener_t *listener, const int64_t configuration) {
    const char name[10] = "Hi";
    const char *names[2];
    names[0] = name;
    dds_qset_partition(qos, static_cast<unsigned>(1), names);
}