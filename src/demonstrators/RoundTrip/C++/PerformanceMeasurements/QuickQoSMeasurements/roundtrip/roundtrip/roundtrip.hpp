#ifndef ROUNDTRIP_H
#define ROUNDTRIP_H

#include "performance/performance_measurements.hpp"

#include "PerformanceData.h"
#include "dds/dds.h"
#include <chrono>

class QoSManager;

/**
 * @brief RoundTrip contains the implementation of the roundtrip
 *
 */
class RoundTrip {
  public:
    RoundTrip(const unsigned int id, const unsigned int totalDevices, const unsigned int roundTrips);
    ~RoundTrip();

    void initialise(QoSManager *qos);

    int getTime() const { return _timer.getTime<std::chrono::microseconds>(); };

    void run();

    // int getMemoryUsage() const { return PerformanceMeasurements::getMemoryUsage(); }
    // float getCPUusage() const { return PerformanceMeasurements::getCPUtime(); }

  private:
    const unsigned int _id;
    const unsigned int _totalDevices;
    unsigned int _totalRoundtrips;

    bool _running;
    unsigned int _roundTrips;

    PerformanceData_Msg _msg;
    dds_entity_t _writer;
    dds_entity_t _reader;
    dds_entity_t _participant;
    dds_entity_t _topicWrite, _topicRead;

    void *_messageMemory[1];

    PerformanceMeasurements _timer;
};

#endif // ROUNDTRIP_H