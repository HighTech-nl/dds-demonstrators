#include <chrono>
#include <iostream>
#include <thread>

#include "flood.hpp"

void errorMessage();

/**
 * @brief main contains the main code for the flood project
 *
 * @param argc number of arguments
 * @param argv the arguments of the function
 * @return int endstate of the application
 */
int main(int argc, char const *argv[]) {
    if (argc != 4) {
        errorMessage();
        return -1;
    }
    unsigned int id;
    unsigned int totalDevices;
    unsigned int totalFloodMessages;
    try {
        id = std::stoul(argv[1]);
        totalDevices = std::stoul(argv[2]);
        totalFloodMessages = std::stoul(argv[3]);
        if (id == 0 || id > totalDevices || totalDevices <= 1) {
            errorMessage();
            return -1;
        }
    } catch (const std::exception &e) {
        errorMessage();
        return -1;
    }
    Flood flood(id, totalDevices, totalFloodMessages);
    flood.run();

    const auto totalFloodTime = flood.getTime();

    if (id != 1) {
        std::cout << "Flood finished, see device 1 for measurements\n\n";
        return 0;
    }

    std::cout << "Flood finished!\n"
              << "Total execution time for " << totalFloodMessages << " messages: " << totalFloodTime
              << " microseconds\n"
              << "Average time per flood message: " << totalFloodTime / static_cast<double>(totalFloodMessages)
              << " microseconds\n"
              << "Time for each message divided by the number of devices: "
              << (totalFloodTime / static_cast<double>(totalFloodMessages)) / totalDevices << " microseconds\n\n";

    return 0;
}

/**
 * @brief errorMessage contains the fault message for wrong parameters
 *
 */
void errorMessage() {
    std::cerr << "Wrong input for this application!\n\n"
              << "Format: ./Flood <device ID> <Total devices> <Messages>\n\n"
              << "<device ID>: The ID of the device within the flood loop, must be unique, > 0 and <= total devices\n"
              << "<Total devices>: The amount of devices in the flood loop (the amount of flood programs running)\n"
              << "<Messages>: Amount of flood messages to send/receive\n\n"
              << "Note: Only the <device ID> is unique! The other 2 parameters MUST be equal for each flood program!\n"
              << "Note: Device ID 1 initiates the flood. Therefore, device ID 1 must be started last!\n"
              << "Note: <Messages> should be equal for all applications";
}