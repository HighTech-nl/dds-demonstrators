#! /usr/bin/env python3

import sys, time, os
import cdds as dds
from _aux import EVERY, process_from, send_data, demo_out_in, M_IN, M_OUT
from datetime import datetime

ReceivingTopic = sys.argv[1] if len(sys.argv) >1 else M_OUT
SendingTopic   = sys.argv[2] if len(sys.argv) >2 else M_IN

LOOP_SLEEP = 6

def plus_1(v):
    global sender
    send_data(sender, v+1)

def on_receive_data(listner):
    msg,c = process_from(listner, plus_1)

if __name__ == '__main__':
    global sender, val
    val=None

    rt = dds.Runtime()
    dp = dds.Participant(0)

    t_out = dds.FlexyTopic(dp,  SendingTopic)
    t_in  = dds.FlexyTopic(dp,  ReceivingTopic)
    sender   = dds.FlexyWriter(dp, t_out, [dds.Reliable(), dds.KeepLastHistory(10)])
    receiver = dds.FlexyReader(dp, t_in, on_receive_data, [dds.Reliable(), dds.KeepLastHistory(10)])

    demo_out_in(SendingTopic, ReceivingTopic)
    print("start listening")

    while True:
        time.sleep(LOOP_SLEEP)
