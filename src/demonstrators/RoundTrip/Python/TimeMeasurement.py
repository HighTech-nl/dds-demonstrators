from datetime import datetime
from datetime import timedelta

class TimeMeasurement:
    def __init__(self):
        self.measurement_started = False

    def start(self):
        if (not self.measurement_started):
            self.start_time = datetime.now()
            self.measurement_started = True
        else:
            print("measurement already started and not stopped!!")

    def stop(self):
        if (self.measurement_started):
            self.end_time = datetime.now()
            self.measurement_started = False
        else:
            print("no measurement started!!")

    def getMeasurement(self):
        return (self.end_time - self.start_time)/timedelta(microseconds=1)

