from unittest import TestCase
import unittest
import inspect
import misc
import dds_library
import caccess

class TestingTypes(TestCase):
    """
    This class contains tests for misc.py and the bindings between
    ctypes and native python types.
    """
    def test_attrs(self):
        """
        Tests whether all the data types have
        all required attributes.
        """
        types = [t for _, t in inspect.getmembers(misc) if inspect.isclass(t) and t.__module__ == 'misc']
        for t in types:
            self.assertTrue(hasattr(t, 'ctype'))
            self.assertTrue(hasattr(t, 'default'))
            self.assertTrue(hasattr(t, 'op_adr'))

    def test_keys(self):
        """
        Tests whether BUILTINS only contain types that have correspondong bindings.
        """
        self.assertEqual(set(misc.native_dict.keys()), set(misc.BUILTINS))
    
    def test_values(self):
        """
        Tests whether all the bindings are actually implemented.
        """
        types = [t for _, t in inspect.getmembers(misc) if inspect.isclass(t) and t.__module__ == 'misc']
        values = list(misc.native_dict.values())
        for v in values:
            self.assertTrue(v in types)


class TestingDomain(TestCase):
    """
    This class contains tests for the Domain object.
    """
    def test_created(self):
        """
        This functions tests whether the underlying participant is created.
        """
        p = dds_library.Domain(1)
        ret = caccess.delete(p.domain_id)
        self.assertEqual(ret, 0)

    def test_int(self):
        """
        Testing magic method __int__()
        """
        p = dds_library.Domain(1)
        self.assertEqual(p.domain_id, int(p))


class TestingListener(TestCase):
    """
    This class contains tests for the Listener object.
    """
    def test_int(self):
        """
        Testing magic method __int__()
        """
        l = dds_library.Listener()
        self.assertEqual(l.id, int(l))


class TestingTopic(TestCase):
    """
    This class tests the implementation of the @topic decorator.
    """
    def test_ctype(self):
        """
        This fucntion tests whether the correct ctype is created for the fields.
        """
        domain = dds_library.Domain(0)

        @dds_library.topic(domain)
        class Data:
            def __init__(self, **kwargs):
                self.id = int
                self.name = str

        fields = Data.ctype._fields_
        for a, v in fields:
            if a == 'id':
                self.assertEqual(v, misc.native_dict[int].ctype)
            elif a == 'name':
                self.assertEqual(v, misc.native_dict[str].ctype)

    def test_python_types(self):
        """
        this function tests the python type inference of topic.
        """
        domain = dds_library.Domain(0)

        @dds_library.topic(domain)
        class Data:
            def __init__(self, **kwargs):
                self.id = int
                self.name = str
        
        for a, v in Data.python_types:
            if a == 'id':
                self.assertEqual(v, int)
            elif a == 'name':
                self.assertEqual(v, str)


    def test_defaults(self):
        """
        This function tests the default values of topics.
        """
        domain = dds_library.Domain(0)

        @dds_library.topic(domain)
        class Data:
            def __init__(self, **kwargs):
                self.id = int
                self.name = str

        d = Data()

        self.assertEqual(d.id, misc.native_dict[int].default)
        self.assertEqual(d.name, misc.native_dict[str].default)

class TestingWriter(TestCase):
    """
    This class contains tests for the Writer object.
    """
    def test_id(self):
        """
        Tests the implementation of __int__(self).
        """
        domain = dds_library.Domain(0)

        @dds_library.topic(domain)
        class Data:
            def __init__(self, **kwargs):
                self.id = int
                self.name = str

        writer = dds_library.Writer(Data)
        self.assertEqual(writer.id, int(writer))


class TestingReader(TestCase):
    """
    This class contains tests for the Reader object.
    """
    def test_id(self):
        """
        Tests the implementation of __int__(self).
        """
        domain = dds_library.Domain(0)

        @dds_library.topic(domain)
        class Data:
            def __init__(self, **kwargs):
                self.id = int
                self.name = str

        reader = dds_library.Reader(Data, 10)
        self.assertEqual(reader.id, int(reader))

    
    def test_buffer(self):
        """
        Tests the buffer implementation.
        """
        domain = dds_library.Domain(0)

        @dds_library.topic(domain)
        class Data:
            def __init__(self, **kwargs):
                self.id = int
                self.name = str

        buff_len = 10

        reader = dds_library.Reader(Data, buff_len)
        self.assertEqual(buff_len, len(reader))

unittest.main()