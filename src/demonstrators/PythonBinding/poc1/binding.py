import ctypes
from ctypes import cdll, CDLL, Structure
from os.path import join

import faulthandler
faulthandler.enable()

LIBPATH = "/usr/local/lib"

cyclone = cdll.LoadLibrary(join(LIBPATH, "libddsc.so"))
# This is only used because of the json IDL. It will go away when the 
# IDL issue is solved.
otherlib = CDLL(join(LIBPATH, "libddstubs.so"))

# The DDS library functions that are supported at the moment.
create_participant = cyclone.dds_create_participant
create_topic = cyclone.dds_create_topic
create_writer = cyclone.dds_create_writer
write = cyclone.dds_write
create_listener = cyclone.dds_create_listener
create_reader = cyclone.dds_create_reader
take = cyclone.dds_take


# --------------------------------------------------------------------------------------
class Key:
    def __init__(self, t: type):
        self.t = t

class Keydescr(Structure):
    _fields_=[
        ("m_name", ctypes.c_char_p),
        ("m_index", ctypes.c_uint32)
    ]

class Descriptor(Structure):
    _fields_=[
        ("m_size", ctypes.c_uint32),
        ("m_align", ctypes.c_uint32),
        ("m_flagset", ctypes.c_uint32),
        ("m_nkeys", ctypes.c_uint32),
        ("m_typename", ctypes.c_char_p),
        ("m_keys", ctypes.POINTER(Keydescr)),
        ("m_nops", ctypes.c_uint32),
        ("m_ops", ctypes.POINTER(ctypes.c_uint32)),
        ("m_meta", ctypes.c_char_p)
    ]

p_Descriptor = ctypes.POINTER(Descriptor)

# This is the lame data type
class LameTopic(Structure):
    _fields_= [
        ("key", ctypes.c_char_p),
        ("value", ctypes.c_char_p)
    ]
    def __repr__(self):
        return f"OBJECT | key: {self.key.decode()}, value: {self.value.decode()} |"

sizezz = ctypes.sizeof(LameTopic)

# Here we recreate the descriptor run-time
ops = (ctypes.c_uint32 * 5)(*[17104897, 0, 17104896, 8, 0])

kd = Keydescr("key".encode(), 0)

topic_descriptor = Descriptor(
    sizezz,
    ctypes.sizeof(ctypes.c_char_p),
    1,
    1,
    "dds::bit::SKeySValue".encode(),
    ctypes.cast((Keydescr*1)(kd), ctypes.POINTER(Keydescr)),
    3,
    ops,
    "<MetaData version=\"1.0.0\"><Module name=\"dds\"><Module name=\"bit\"><Struct name=\"SKeySValue\"><Member name=\"key\"><String/></Member><Member name=\"value\"><String/></Member></Struct></Module></Module></MetaData>".encode()
)
# ===============================================



class DDSKeyValue(Structure):
    """
    The json topic. Will be removed after fixing IDLs.
    """
    _fields_ = [('key', ctypes.c_char_p), ('value', ctypes.c_char_p)]
    def __repr__(self):
        return f"OBJECT | key: {self.key.decode()}, value: {self.value.decode()} |"

class SampleInfo(Structure):
    """
    Hard coded sample info for now.
    """
    _fields_ = [('sample_state', ctypes.c_uint),
                ('view_state', ctypes.c_uint),
                ('instance_state', ctypes.c_uint),
                ('valid_data', ctypes.c_bool),
                ('source_timestamp', ctypes.c_int64),
                ('instance_handle', ctypes.c_uint64),
                ('pubblication_handle', ctypes.c_uint64),
                ('disposed_generation_count', ctypes.c_uint32),
                ('no_writer_generation_count', ctypes.c_uint32),
                ('sample_rank', ctypes.c_uint32),
                ('generation_rank', ctypes.c_uint32),
                ('absolute_generation_rank', ctypes.c_uint32)]


class Participant:
    """
    Representing a DDS endpoint.
    """
    def __init__(self, id:int):
        self.id = create_participant(id, ctypes.cast(None, ctypes.c_void_p), ctypes.cast(None, ctypes.c_void_p))
    
    def __int__(self):
        return self.id

    def __str__(self):
        return f"Participant object with id: {self.id.__str__()}"


class Topic:
    """
    Class for representing topic.
    """
    def __init__(self, name:str, participant:Participant):
        self.participant = participant
        self.name = name
        self.id = create_topic(ctypes.c_int32(participant), ctypes.byref(topic_descriptor), name.encode(), None, None)

    def __int__(self):
        return self.id

    def __str__(self):
        return f"Topic object with id: {self.id.__str__()}"


class Writer:
    def __init__(self, participant:Participant, topic:Topic):
        self.participant = participant
        self.topic = topic
        self.id = create_writer(int(participant), int(topic), None, None)

    def __str__(self):
        return f"Writer object with id: {self.id.__str__()}"

    def __int__(self):
        return self.id

    def __call__(self, msg):
        return write(self.__int__(), ctypes.byref(msg))

class Listener:
    def __init__(self):
        self.id = create_listener(None)

    def __str__(self):
        return f"Listener object with id: {self.id.__str__()}"

    def __int__(self):
        return self.id

class Reader:
    def __init__(self, participant:Participant, topic:Topic, listener:Listener=Listener(), size:int=10):
        self.participant = participant
        self.topic = topic
        self.listener = listener
        # void pointer array, new items will be stored here after calling __call__(self, *)
        self.buffer = (ctypes.c_void_p * size)()
        self.ivec = (SampleInfo * size)()
        self.id = create_reader(int(participant), int(topic), None, int(listener))
        self.size = size

    def __len__(self):
        return self.size

    def __call__(self, as_type:type=None):
        """
        Reads from the buffer, and returns a list of objects, casted to the specified type. 
        """
        n_read = take(ctypes.c_int32(self.id), self.buffer, self.ivec, ctypes.c_ulong(self.size), ctypes.c_uint32(self.size))
        rval = self.buffer[:n_read]
        if as_type is not None:
            rval = list(map(lambda x: ctypes.cast(x, ctypes.POINTER(as_type))[0], rval))
        return rval
            

    def __str__(self):
        return f"Reader object with id: {self.id}"

    def __int__(self):
        return self.id
