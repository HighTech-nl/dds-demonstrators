#!/usr/bin/env python3

import cdds
import time, datetime
import numpy as np
import pandas as pd
import xscope_config
import tkinter as tk
from tkinter import messagebox
import sys
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick


class xScope:
    def __init__(self):
        self.domain_id = xscope_config.dds_info['domain_id']
        self.topics_str = xscope_config.dds_info['topic']
        self.flexy_topics = []
        self.data_readers = []
        self.df_columns = ['Time']
        self.df = pd.DataFrame([], columns= self.df_columns)



    def store_data(self, topic_namestr, object_dump):
        del object_dump["py/object"]
        # update the columns
        fields = list(object_dump.keys())
        column_list = [ topic_namestr + '.' + key for key in fields ]
        # self.df_columns = self.df_columns + column_list
        # self.df_columns = list(dict.fromkeys(self.df_columns)) #remove duplicates

        # write values to df
        value_list = list(object_dump.values())
        dict_to_write = dict(zip(column_list, value_list))
        # time_format = '%Y-%m-%d %H:%M:%S.%f'
        time_format = '%H:%M:%S.%f'
        dict_to_write["Time"] = datetime.datetime.utcnow().strftime(time_format)[:-3]

        # print(dict_to_write)

        self.df = self.df.append([dict_to_write],ignore_index=True, sort=True)

    def show_traffic(self):
        runtime = cdds.Runtime()
        domain_participant = cdds.Participant(self.domain_id)

        for topic in self.topics_str:
            t = cdds.FlexyTopic(domain_participant, topic)
            self.flexy_topics.append(t)

        for flexy_topic in self.flexy_topics:
            dr = cdds.FlexyReader(domain_participant, flexy_topic, self.data_available, [cdds.Reliable(), cdds.KeepLastHistory(10)])
            dr.on_liveliness_changed(self.liveliness_changed)

        try:
            while True:
                time.sleep(60)
        except:
            cols = self.df.columns.tolist()
            cols.remove("Time")
            cols.insert(0,"Time")
            self.df = self.df[cols]
            self.df.to_csv("xScope_Recording.csv")
            self.visualize_data()
            print("xScope has been stopped, for loggings see .csv file")


    def prepare_data_store(self):
        pass

    def visualize_data(self):
        if self.df.empty == True:
            print(" No data to draw a graph!")
        else:
            # plot albert's implementation - the loop example
            if 'Master_Out.val' in self.df.columns:
                plt.scatter(self.df['Time'], self.df['Master_Out.val'])
                plt.xticks(
                            rotation=45,
                            horizontalalignment='right',
                            fontweight='light',
                            fontsize='small'
                )
                plt.show()

    def data_available(self,r):
        # reader >> listener called
        samples = r.take(cdds.all_samples())
        for s in samples:
            if s[1].valid_data:
                print('Topic[{1}]>> {0})'.format(s[0], r.flexy_topic.namestr))
                self.store_data(r.flexy_topic.namestr, s[0])

    def liveliness_changed(self,r, e):
        print(">>>>> Changed Liveliness!!")

def Message_window(xScope):
    root = tk.Tk()
    root.withdraw()
    s = " - "
    PopUp = tk.messagebox.askyesno('Scope Settings',
                                      'Domain ID: ' + str(xScope.domain_id) + '\n\n'
                                      + 'Topic(s): '+ s.join(xScope.topics_str) + '\n\n' +
                                      'Do you want to continue?')
    if PopUp == False:
        print("xScope has been stopped, no loggings have been made")
        sys.exit()
    else:
        root.destroy()
        return

if __name__ == "__main__":
    print("xScope running")
    xScope = xScope()
    Message_window(xScope)
    xScope.prepare_data_store()
    xScope.show_traffic()