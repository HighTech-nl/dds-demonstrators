#include "resource_usage.hpp"

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "sys/vtimes.h"

#include <chrono>
#include <thread>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream> // remove_if

int ResourceUsage::_numProcessors{0};

/**
 * @brief Construct a new ResourceUsage object
 *
 * @param applicationPID the pid of the application that needs to be measured (keep empty for the running application),
 * by default: "self"
 */
ResourceUsage::ResourceUsage(const unsigned int applicationPID)
    : _cpuUsage1{0}, _procTimes1{0}, _initialized{false},
      _applicationPID{applicationPID ? std::to_string(applicationPID) : "self"}, _timer{
                                                                                     std::chrono::milliseconds{1000}} {}

/**
 * @brief initializes the CPU usage measurement
 *
 */
void ResourceUsage::initializeCPUusage() { _numProcessors = countParameter("/proc/cpuinfo", "processor"); }

/**
 * @brief reads the CPU usage and returns it
 *
 * @return percentage the measured CPU usage in percentage
 */
ResourceUsage::percentage ResourceUsage::getCPUusage() {
    if (!_initialized) {
        initializeCPUusage();
        _cpuUsage1 = getTotalCPUusage();
        _procTimes1 = getProcessCPUusage();
        _initialized = true;
        _timer.start(false);
    }
    if (_timer.elapsed()) {
        int procTimes2;
        int totalCPUusage2;

        totalCPUusage2 = getTotalCPUusage();

        procTimes2 = getProcessCPUusage();

        double result =
            (_numProcessors * (procTimes2 - _procTimes1) * 100.0) / static_cast<double>(totalCPUusage2 - _cpuUsage1);
        _timer.restart();
        _procTimes1 = procTimes2;
        _cpuUsage1 = totalCPUusage2;
        return result;
    }
    return -1;
}

/**
 * @brief reads the total CPU usage of the system
 *
 * @return int the total CPU usage of the system
 */
int ResourceUsage::getTotalCPUusage() {
    std::fstream file("/proc/stat", std::fstream::in);
    if (file.good()) {
        std::string totalCPU;
        getline(file, totalCPU);
        std::vector<int> vectored = stringToVector(deleteNonNumericAndSpace(totalCPU));
        int totalCPUusage = vectored.at(0) + vectored.at(2) + vectored.at(3) + vectored.at(5);
        return totalCPUusage;
    }
    return {};
}

/**
 * @brief reads the CPU usage of the application
 *
 * @return int the CPU usage of the application
 */
int ResourceUsage::getProcessCPUusage() const {
    std::string location = "/proc/" + _applicationPID + "/stat";
    std::fstream file(location, std::fstream::in);
    if (file.good()) {
        std::string cpuProcess;
        getline(file, cpuProcess);
        std::vector<int> vectored = stringToVector(deleteNonNumericAndSpace(cpuProcess));
        if (vectored.size() < 20) {
            return {};
        }
        int procTimes = vectored.at(12) + vectored.at(13);
        return procTimes;
    }
    return {};
}

/**
 * @brief parses a line in a file from linux
 *
 * @param line the line that needs to be parsed
 * @return int the number that is read on the parsed line
 */
int ResourceUsage::parseLine(const std::string &line) {
    std::string result;
    for (auto e : line) {
        if (e >= '0' && e <= '9') {
            result += e;
        }
    }
    if (result.size() > 0) {
        return std::stoi(result);
    } else {
        return {};
    }
}

/**
 * @brief returns the physical memory usage of an application
 *
 * @param applicationPID the Process ID of the application
 * @return ResourceUsage::kilobytes the physical memory usage in kilobytes
 */
ResourceUsage::kilobytes ResourceUsage::getPhysicalMemoryUsage(const unsigned int applicationPID) {
    if (applicationPID != 0) {
        return getPhysicalMemoryUsageStr(std::to_string(applicationPID));
    } else {
        return getPhysicalMemoryUsageStr();
    }
}

/**
 * @brief returns the virtual memory usage of an application
 *
 * @param applicationPID the Process ID of the application
 * @return ResourceUsage::kilobytes the virtual memory usage in kilobytes
 */
ResourceUsage::kilobytes ResourceUsage::getVirtualMemoryUsage(const unsigned int applicationPID) {
    if (applicationPID != 0) {
        return getVirtualMemoryUsageStr(std::to_string(applicationPID));
    } else {
        return getVirtualMemoryUsageStr();
    }
}

/**
 * @brief returns the virtual memory usage of an application
 *
 * @param applicationPID a string containing the application PID or "self" when reading the virtual memory usage of the
 * process running this function, by default: "self"
 * @return ResourceUsage::kilobytes the virtual memory usage of the application
 */
ResourceUsage::kilobytes ResourceUsage::getVirtualMemoryUsageStr(const std::string &applicationPID) {
    std::string location{"/proc/" + applicationPID + "/status"};

    return getParameter(location, "VmSize:");
}

/**
 * @brief returns the physical memory usage of an application
 *
 * @param applicationPID a string containing the application PID or "self" when reading the physical memory usage of the
 * process running this function, by default: "self"
 * @return ResourceUsage::kilobytes the physical memory usage of the application
 */
ResourceUsage::kilobytes ResourceUsage::getPhysicalMemoryUsageStr(const std::string &applicationPID) {
    std::string location{"/proc/" + applicationPID + "/status"};

    return getParameter(location, "VmRSS:");
}

/**
 * @brief getParameter gets a parameter from a configuration file
 *
 * @param location the file location that needs to be parsed
 * @param parameterName the name of the parameter of which the value is returned
 * @return int the found value of the parameter
 */
int ResourceUsage::getParameter(const std::string &location, const std::string &parameterName) {
    std::fstream file(location, std::fstream::in);

    std::string line;
    size_t size = parameterName.size();

    while (getline(file, line)) {
        if (line.size() > size && line.substr(0, size) == parameterName) {
            break;
        }
    }
    return parseLine(line);
}

/**
 * @brief counts how often a name occurs in a file
 *
 * @param location the location of the file
 * @param parameterName the name of the parameter that needs to be found in the file
 * @return int the number of times the parameterName occured in the file
 */
int ResourceUsage::countParameter(const std::string &location, const std::string &parameterName) {
    std::fstream file(location, std::fstream::in);

    std::string line;
    size_t size = parameterName.size();
    int counted = 0;

    while (getline(file, line)) {
        if (line.size() > size && line.substr(0, size) == parameterName) {
            counted++;
        }
    }
    return counted;
}

/**
 * @brief deletes non numeric values and spaces from the input
 *
 * @param input a string of which non numeric and space values are removed
 * @return std::string the input without numeric and space values
 */
std::string ResourceUsage::deleteNonNumericAndSpace(const std::string &input) {
    std::string result{input};
    result.erase(std::remove_if(result.begin(), result.end(), &noDigitOrSpace));
    while (result.back() == ' ') {
        result.pop_back();
    }
    return result;
}

/**
 * @brief stringToVector converts a string to a vector
 *
 * @param input a string that needs to be converted to a vector
 * @return std::vector<int> the converted string
 */
std::vector<int> ResourceUsage::stringToVector(const std::string &input) {
    std::istringstream retrieveInt(input);
    int intValue;
    std::vector<int> result;
    while (retrieveInt >> intValue) {
        result.push_back(intValue);
    }
    return result;
}