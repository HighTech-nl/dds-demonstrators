#include "Test.hpp"

std::mutex Test::_write_network_received_mutex{};
std::mutex Test::_write_traffic_received_mutex{};
std::mutex Test::_write_network_sent_mutex{};
std::mutex Test::_write_traffic_sent_mutex{};
/**
 * @brief Construct a new Test:: Test object
 * 
 * Creates a CSV file to write measurements to.
 * 
 * @param filename name of the file to create and write to.
 */
Test::Test(std::string filename_id): _traffic_received_file_name{"traffic_received_"+filename_id+".csv"},
_network_received_file_name{"network_received_"+filename_id+".csv"},
_traffic_sent_file_name{"traffic_sent_"+filename_id+".csv"}, 
_network_sent_file_name{"network_sent_"+filename_id+".csv"}{
    if(filename_id != "startupstart"){
    initFiles();
    }
}

void Test::initFiles(){
        remove(_traffic_received_file_name.c_str());
        remove(_network_received_file_name.c_str());
        remove(_traffic_sent_file_name.c_str());
        remove(_network_sent_file_name.c_str());
        std::string column_headers = "message_id;sent_by;received_by;topic;milli";
        WriteToFile(column_headers, _traffic_received_file_name);
        WriteToFile(column_headers, _network_received_file_name);
        WriteToFile(column_headers, _traffic_sent_file_name);
        WriteToFile(column_headers, _network_sent_file_name);
}
/**
 * @brief Function to write messaging measurement to the measurements CSV file
 * 
 * @param message_id ID of the message.
 * @param sent_by The sender of the message.
 * @param received_by The receiver of the message.
 * @param topic The topic to which the message belongs.
 * @param time The time of sending the message.
 */
void Test::WriteMeasurement(int message_id, int sent_by, int received_by, std::string topic, Clock::time_point time, int type){
    if(type ==  1){
        std::lock_guard<std::mutex> write_lock(_write_traffic_received_mutex);
        std::string line = std::to_string(message_id)+";"+std::to_string(sent_by)+";"+std::to_string(received_by)+";"+topic+";"+getMiliFromTime(time);
        WriteToFile(line, _traffic_received_file_name); 
    }
    else if(type ==  2){
        std::lock_guard<std::mutex> write_lock(_write_network_received_mutex);
        std::string line = std::to_string(message_id)+";"+std::to_string(sent_by)+";"+std::to_string(received_by)+";"+topic+";"+getMiliFromTime(time);
        WriteToFile(line, _network_received_file_name); 
    }
    else if(type ==  3){
        std::lock_guard<std::mutex> write_lock(_write_traffic_sent_mutex);
        std::string line = std::to_string(message_id)+";"+std::to_string(sent_by)+";"+std::to_string(received_by)+";"+topic+";"+getMiliFromTime(time);
        WriteToFile(line, _traffic_sent_file_name);
    }
    else if(type ==  4){
        std::lock_guard<std::mutex> write_lock(_write_network_sent_mutex);
        std::string line = std::to_string(message_id)+";"+std::to_string(sent_by)+";"+std::to_string(received_by)+";"+topic+";"+getMiliFromTime(time);
        WriteToFile(line, _network_sent_file_name);
    }
}
/**
 * @brief Function to write a string to the measurements file.
 * 
 * @param line Line to write to the file
 */
void Test::WriteToFile(std::string line, std::string file_name){
    std::fstream measurementsFile(file_name, std::fstream::in | std::fstream::out | std::fstream::app);
    measurementsFile << line << "\n";
    measurementsFile.close();
}
/**
 * @brief Function to get milliseconds from epoch till the input time
 * 
 * @param time Input timestamp
 * @return std::string string which can be written to file containing the 
 * the time since epoch in milliseconds
 */
std::string Test::getMiliFromTime(Clock::time_point time) {
    auto duration = time.time_since_epoch();
    int millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
    return std::to_string(millis);
}
/**
 * @brief Function to get the difference between time points in milliseconds
 * 
 * @param start_time the start time point
 * @param end_time the end time point
 * @return std::string 
 */
std::string Test::getTimeDifference(Clock::time_point start_time, Clock::time_point end_time) {
    return std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count());
}