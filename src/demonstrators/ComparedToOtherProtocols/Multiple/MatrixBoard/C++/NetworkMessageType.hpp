#ifndef NETWORKMESSAGETYPE_HPP
#define NETWORKMESSAGETYPE_HPP

enum NetworkMessageType
{
    CONNECTED,
    ONLINE,
    DISCONNECTED,
};

#endif