#ifndef ZMQCOMMUNICATION_HPP
#define ZMQCOMMUNICATION_HPP

#include "Communication.hpp"
#include <type_traits>
#include <zmq.hpp>
#include <mutex>


/**
 * @brief ZMQ communcation class implements
 * abstract Communication class.
 * 
 */
class ZMQCommunication: public Communication{
private:
    std::string _server_address;
    std::string _server_publish_address;
    std::string _own_address;
    std::string _topic_write;
    std::string _topic_network;
    zmq::context_t _context;
    zmq::socket_t _sub_socket;
    zmq::socket_t _pub_socket;
    zmq::socket_t _network_pub_socket;
    std::map<std::string, std::string> _board_addresses;
    static std::mutex _addresses_mutex;

public:
    ZMQCommunication(std::string id, std::string server_address, std::string server_publish_address, std::string own_address);
    ~ZMQCommunication() = default;
    void connectToNetwork(Network_Message initMsg, int id);
    static void *initialConnectionCheckerThread(void* thread_arg);
    void sendTrafficMessage(Traffic_Message msg);
    void sendNetworkMessage(Network_Message msg);
    MessageReturnStruct receive(int id);
    void subscribe(std::string topic, std::string address = "");
    void unsubscribe(std::string topic);
    zmq::message_t generateMessageFromString(std::string message);
    template <typename StructType>
    zmq::message_t generateMessageFromStruct(StructType structure);
};

#endif