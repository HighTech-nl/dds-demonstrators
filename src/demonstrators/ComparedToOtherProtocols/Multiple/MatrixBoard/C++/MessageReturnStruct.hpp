#ifndef MESSAGERETURNSTRUCT_HPP
#define MESSAGERETURNSTRUCT_HPP

#include <string>

struct MessageReturnStruct {
    std::string topic;
    Traffic_Message traffic_struct;
    Network_Message network_struct;
};

#endif