#ifndef INITCONNECTIONARGSTRUCT_HPP
#define INITCONNECTIONARGSTRUCT_HPP

template <typename CommunicationType> 
struct InitConnectionArgStruct { 
  std::atomic<bool>* connected; 
  int id;
  CommunicationType* communication; 
};

#endif