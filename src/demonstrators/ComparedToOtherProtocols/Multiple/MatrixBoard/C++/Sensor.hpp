#ifndef SENSOR_HPP
#define SENSOR_HPP
#include <random>
/**
 * @brief Class representing the sensor of the matrix board
 *  which measures traffic intensity
 * 
 */
class Sensor
{
private:
    /* data */
public:
    Sensor() = default;
    ~Sensor() = default;
    int measure();
};
#endif 