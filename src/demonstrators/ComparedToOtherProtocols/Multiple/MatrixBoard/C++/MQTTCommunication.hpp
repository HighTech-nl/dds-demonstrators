

#ifndef MQTTCOMMUNICATION_HPP
#define MQTTCOMMUNICATION_HPP

#include "mqtt/client.h"
#include "Communication.hpp" 
#include <sstream>
/**
 * @brief MQTT communication class implements
 * abstract communication class.
 * 
 */
class MQTTCommunication: public Communication{
private:
    int _qos;
    mqtt::async_client _client;
    mqtt::topic _topic_network;
    mqtt::topic _topic_write;
public:
    MQTTCommunication(std::string id, std::string server_address, int qos);
    ~MQTTCommunication() = default;
    void sendTrafficMessage(Traffic_Message msg);
    void sendNetworkMessage(Network_Message msg);
    void connectToNetwork(Network_Message initMsg, int id);
    MessageReturnStruct receive(int id);
    void subscribe(std::string topic, std::string address = "");
    void unsubscribe(std::string topic);
    Traffic_Message stringToTrafficStruct(std::string dataString);
    Network_Message stringToNetworkStruct(std::string dataString);
    std::string trafficStructToString(Traffic_Message trafficStruct);
    std::string networkStructToString(Network_Message networkStruct);
    std::vector<std::string> csvStringToVector(std::string dataString);
};

#endif
