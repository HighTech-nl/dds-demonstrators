#include "Display.hpp"
/**
 * @brief Construct a new Display:: Display object
 * Starting sign is always blank.
 */
Display::Display():_sign{BLANK}{
}
/**
 * @brief Function to set the sign on the display.
 * 
 * @param sign Sign to be displayed.
 */
void Display::setSign(Sign sign){
    _sign = sign;
}
/**
 * @brief Function to get currently displayed sign.
 * 
 * @return Sign 
 */
Sign Display::getSign(){
    return _sign;
}