#!/usr/bin/env python3

import zmq

import sys

ReceievePort =  int(sys.argv[1]) if len(sys.argv) > 1 else 8881 # take the receive port from command line input or use the default one
SendPort =  int(sys.argv[2]) if len(sys.argv) > 2 else 8882
next_hub_IP = sys.argv[3] if len(sys.argv) > 3 else "localhost"


def main():
    

    
    context = zmq.Context()
    sender = context.socket(zmq.PAIR)
    sender.connect("tcp://"+next_hub_IP+":%s" % SendPort)
    
    context = zmq.Context()
    receiver = context.socket(zmq.PAIR)                              # XXX GAM: hardcoded, use also other network-patterns
    receiver.bind("tcp://*:%s" % ReceievePort)
               
    while True:
        msg = receiver.recv().decode()
        sender.send_string(msg)
    pass



if __name__ == "__main__":
    main()
