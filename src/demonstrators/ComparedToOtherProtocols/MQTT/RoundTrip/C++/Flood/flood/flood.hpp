#ifndef FLOOD_H
#define FLOOD_H

#include "performance/performance_measurements.hpp"

#include "mqtt/client.h"

/**
 * @brief Flood contains the implementation of a flood loop with DDS
 *
 */
class Flood {
  public:
    Flood(const unsigned int id, const unsigned int totalDevices, const unsigned int floodMessages, std::string server_address, const int qos);
    ~Flood() = default;

    int getTime() const { return _timer.getTime<std::chrono::microseconds>(); };

    void run();

  private:
    const unsigned int _id;
    const unsigned int _totalDevices;
    unsigned int _totalFloodMsg;

    bool _running, _message;
    unsigned int _floodSend;
    unsigned int _floodReceived;

    mqtt::async_client _client;

    mqtt::topic _topicRead, _topicWrite;

    PerformanceMeasurements _timer;

    void initialise();
    void runMaster();
    void runSlave();
};

#endif // FLOOD_H