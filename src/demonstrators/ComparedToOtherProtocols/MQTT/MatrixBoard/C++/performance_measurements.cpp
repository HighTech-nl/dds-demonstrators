#include "performance_measurements.hpp"

/**
 * @brief start starts a time measurement
 *
 */
void PerformanceMeasurements::start() { _startingTime = std::chrono::high_resolution_clock::now(); }

/**
 * @brief stop stops a time measurement
 *
 */
void PerformanceMeasurements::stop() { _stoppedTime = std::chrono::high_resolution_clock::now(); }
/**
 * @brief gets the amount of milliseconds between epoch time and starttime and returns value as string
 * 
 */
std::string PerformanceMeasurements::getStartMili() {
    auto duration = _startingTime.time_since_epoch();
    int millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
    return std::to_string(millis);
}
