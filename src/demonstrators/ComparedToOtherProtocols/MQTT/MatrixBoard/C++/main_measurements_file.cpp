#include "measurements_file.hpp"
#include <iostream>

inline bool exists (const std::string& name);

int main(int argc, char const *argv[]) {
    int removed_file;
    removed_file = remove("measurements.csv");
    if(removed_file != 0){
        std::cout << "measurements file could not be deleted." << std::endl;
    }
    //name of file where measurements will be put
    MeasurementsFile performance_file("measurements.csv");
    //sets column headers of measurements file
    std::vector<std::string> measurements{"message_id", "sent_by", "received_by", "topic", "Message", "milli"};
    performance_file.createFile(measurements);

    return 0;
}
inline bool exists (const std::string& name) {
    if (FILE *file = fopen(name.c_str(), "r")) {
        fclose(file);
        return true;
    } else {
        return false;
    }   
}