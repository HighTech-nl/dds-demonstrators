#ifndef SPECIFIC_MATRIXBOARD_H
#define SPECIFIC_MATRIXBOARD_H

#include <string>

#include "MBCData.h"
#include "dds/dds_basics.hpp"
#include <dds/dds.h>

namespace matrixboard {

/**
 * @brief Contains the implementation for communication with matrix boards with a certain ID
 * This class is mainly used for communication with the subsequent matrix boards. This class only
 * allows reading from the topics of the matrix boards it is connected to.
 */
class SpecificMatrixBoard : public DDSBasics<MBCData_Msg> {
  public:
    struct MatrixboardInfo {
        MatrixboardInfo(const unsigned int _topicId, const bool _isWriter) : topicId{_topicId}, isWriter{_isWriter} {}
        unsigned int topicId;
        bool isWriter;
    };
    // Constructor & destructor
    SpecificMatrixBoard(const std::string &generalTopicName, const MatrixboardInfo matrixboardInfo);
    ~SpecificMatrixBoard();

    void initialize();

  private:
    struct LivelinessArguments {
        LivelinessArguments(MatrixboardInfo *_matrixboardInfo, dds_entity_t *_writer)
            : matrixboardInfo{_matrixboardInfo}, writer{_writer} {}
        MatrixboardInfo *matrixboardInfo;
        dds_entity_t *writer;
    };
    const std::string _topicName;
    MatrixboardInfo _matrixboardInfo;
    LivelinessArguments _livelinessArgs;
    dds_entity_t _ddsParticipant;

    static void livelinessCallbackReader(dds_entity_t reader, dds_liveliness_changed_status status, void *arg);
    static void livelinessCallbackWriter(dds_entity_t writer, dds_publication_matched_status_t status, void *arg);
    void configureQoS(dds_qos_t *qos);
    void configureListener(dds_listener_t *listener);
    dds_listener_t *createListener();

    void registerToTopic(const std::string &topicName, const bool createWriter = true, const bool createReader = true);
};

} // namespace matrixboard

#endif // SPECIFIC_MATRIXBOARD_H