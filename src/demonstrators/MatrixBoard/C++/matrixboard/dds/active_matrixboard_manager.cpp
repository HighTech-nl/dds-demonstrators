#include "active_matrixboard_manager.hpp"

#include "MBCData.h"

#include <chrono>
#include <thread>

#include "debugger/debug_topic_manager.hpp"

namespace matrixboard {

/**
 * @brief Constructs a new ActiveMatrixBoardManager object
 *
 * @param participant the DDS participant that this class is going to use for the DDS communication
 * @param id the ID of the matrix board
 * @param topic the topic of the ActiveMatrixBoardManager shared topic
 */
ActiveMatrixBoardManager::ActiveMatrixBoardManager(dds_entity_t *participant, const unsigned int id,
                                                   const std::string &topic)
    : _subsequentMatrixBoards{0, 0}, _id{id}, _baseTopicName{topic}, _activeMB{participant} {}

/**
 * @brief initializes the ActiveMatrixBoardManager class
 *
 */
void ActiveMatrixBoardManager::initialize() {
    _activeMB.registerToTopic(_baseTopicName);
    writeOwnID();
    addMatrixBoards();
    _activeMB.getStatus(); // Remove its own topic and existing MB's from the status
    updateBoards();
}

/**
 * @brief adds matrix boards to the _sharedMatrixBoards map by reading the received messages from DDS
 *
 */
void ActiveMatrixBoardManager::addMatrixBoards() {
    bool running = true;

    DDSActiveMatrixBoard::MessageInfo message;
    while (running) {
        message = _activeMB.readMessage();
        if (message.newMessage) {
            if (_id != message.id) {
                _sharedMatrixBoards.insert(std::make_pair(message.id, message.handle));
            }
            continue;
        } else {
            running = false;
        }
    }
}

/**
 * @brief checkChanges checks the active matrix boards topic if there are changes
 * If there are new matrix boards alive, then they will be added to the map
 * If there are matrix boards removed, they will be removed from the map
 */
bool ActiveMatrixBoardManager::checkChanges() {
    dds_liveliness_changed_status_t statusChanged = _activeMB.getStatus();

    if (statusChanged.alive_count_change < 0) {
        removeMatrixBoard(statusChanged.last_publication_handle);
        return updateBoards();
    }
    if (_activeMB.receivedNewMessage() == true) {
        addMatrixBoards();
        return updateBoards();
    }
    return 0;
}

/**
 * @brief removes a matrix board with a certain handle from the map
 *
 * @param handle the handle of the matrix board that is being removed
 */
void ActiveMatrixBoardManager::removeMatrixBoard(const dds_instance_handle_t handle) {
    for (MatrixboardsMap::iterator iter = _sharedMatrixBoards.begin(); iter != _sharedMatrixBoards.end();) {
        // remove the device from the map
        if (iter->second == handle) {
            iter = _sharedMatrixBoards.erase(iter);
            break;
        }
        ++iter;
    }
}

/**
 * @brief updates the _subsequentMatrixBoards class member variable to the subsequent ID's
 * This function should be executed when the _sharedMatrixBoards map is updated (ID removed or added)
 */
bool ActiveMatrixBoardManager::updateBoards() {
    MatrixboardsMap::iterator iterator = _sharedMatrixBoards.find(_id);
    bool foundFirst = false, foundSecond = false;
    SubsequentMatrixBoards notChangedBoards{_subsequentMatrixBoards};
    while (iterator != --_sharedMatrixBoards.end()) {
        ++iterator;
        if (iterator->first != _id && !foundFirst) {
            _subsequentMatrixBoards.subsequent = iterator->first;
            foundFirst = true;
        } else if (iterator->first != _subsequentMatrixBoards.subsequent) {
            _subsequentMatrixBoards.afterSubsequent = iterator->first;
            foundSecond = true;
            break;
        }
    }
    if (foundFirst == false) {
        _subsequentMatrixBoards.subsequent = 0;
        _subsequentMatrixBoards.afterSubsequent = 0;
    } else if (foundSecond == false) {
        _subsequentMatrixBoards.afterSubsequent = 0;
    }
    if (notChangedBoards == _subsequentMatrixBoards) {
        return 0;
    }
    return 1;
}

} // namespace matrixboard