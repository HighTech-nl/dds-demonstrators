#!/bin/bash
# This script builds all applications at once
# This script should be executed from a build folder that is placed in this directory
cmake -D CUSTOM_MBC=default .. && make -j4
cmake -D CUSTOM_MBC=durability_persistent .. && make -j4
cmake -D CUSTOM_MBC=durability_transient .. && make -j4
cmake -D CUSTOM_MBC=durability_transient_local .. && make -j4
cmake -D CUSTOM_MBC=durability_transient_service_long .. && make -j4
cmake -D CUSTOM_MBC=durability_transient_service_short .. && make -j4
cmake -D CUSTOM_MBC=durability_volatile .. && make -j4
cmake -D CUSTOM_MBC=long_deadline .. && make -j4
cmake -D CUSTOM_MBC=long_lifespan .. && make -j4
cmake -D CUSTOM_MBC=long_liveliness .. && make -j4
cmake -D CUSTOM_MBC=manual_liveliness .. && make -j4
cmake -D CUSTOM_MBC=reliable .. && make -j4
cmake -D CUSTOM_MBC=short_deadline .. && make -j4
cmake -D CUSTOM_MBC=short_lifespan .. && make -j4
cmake -D CUSTOM_MBC=short_liveliness .. && make -j4
cmake -D CUSTOM_MBC=unreliable .. && make -j4