#include "measurements.hpp"

#include "general/timer.hpp"

#include "general/timestampdifference.hpp"

/**
 * @brief Constructs a new Measurements object
 *
 * @param file a pointer to the file where the results will be stored
 * @param id the id of the matrix board application that is run
 */
Measurements::Measurements(FileManager *file, const unsigned int id)
    : _debugTopic{&_participant, id}, _file{file}, _timerStarted{false}, _id{id}, _timer{
                                                                                      std::chrono::milliseconds{1000}} {
    _participant = dds_create_participant(DDS_DOMAIN_DEFAULT, NULL, NULL);
    _debugTopic.initialize();
    _timer.start(false);
}

/**
 * @brief Destroys the Measurements
 *
 */
Measurements::~Measurements() { dds_delete(_participant); }

/**
 * @brief initiates the measurements for the new application
 *
 * @param applicationPID the PID of the new application
 */
void Measurements::startNewApplication(const unsigned int applicationPID) {
    auto findMeasurement = _measurements.find(applicationPID);
    if (findMeasurement == _measurements.end()) {
        MeasuredData data;
        _measurements.insert(std::make_pair(applicationPID, data));
    }
}

/**
 * @brief checks if there are changes on the debug topic
 *
 */
void Measurements::checkChanges() { _debugTopic.checkDebugTopic(); }

/**
 * @brief reads the measurements of the debug topic
 *
 * @param applicationPID the PID of the matrix board application
 */
void Measurements::readMeasurements(const unsigned int applicationPID) {
    processConnectData(applicationPID, _debugTopic.getConnected());
    processDisconnectData(applicationPID, _debugTopic.getDisconnected());
}

/**
 * @brief processes the connect data
 *
 * @param applicationPID the PID of the matrix board application
 * @param message the received message from the debug topic
 */
void Measurements::processConnectData(const unsigned int applicationPID,
                                      const Debug::DebugTopicProcessor::ReceivedMsg &message) {
    auto findMeasurement = _measurements.find(applicationPID);
    if (message.first.size() == 0 || message.second.size() == 0) {
        Logger::print(Logger::LogLevel::ERROR,
                      "Not received all necessary connect data!, replacing this measurement with -9999");
        findMeasurement->second.connectTime.insert(-9999);
        return;
    }
    const size_t firstSize = message.first.size();
    const size_t secondSize = message.second.size();
    int64_t measuredTime;
    for (size_t i = 0; i < firstSize; i++) {
        if (i >= secondSize) {
            measuredTime = TimeStampDifference::timeDifference(message.first.at(i), message.second.at(secondSize - 1));
            findMeasurement->second.connectTime.insert(static_cast<int>(measuredTime));
        } else {
            measuredTime = TimeStampDifference::timeDifference(message.first.at(i), message.second.at(i));
            findMeasurement->second.connectTime.insert(static_cast<int>(measuredTime));
        }
    }
}

/**
 * @brief processes the disconnect data
 *
 * @param applicationPID the PID of the matrix board application
 * @param message the received message from the debug topic
 */
void Measurements::processDisconnectData(const unsigned int applicationPID,
                                         const Debug::DebugTopicProcessor::ReceivedMsg &message) {
    auto findMeasurement = _measurements.find(applicationPID);
    if (message.first.size() == 0 || message.second.size() == 0) {
        Logger::print(Logger::LogLevel::ERROR,
                      "Not received all necessary disconnect data!, replacing this measurement with -9999");
        findMeasurement->second.disconnectTime.insert(-9999);
        return;
    }
    const size_t secondSize = message.second.size();
    int64_t measuredTime;
    for (size_t i = 0; i < secondSize; i++) {
        measuredTime = TimeStampDifference::timeDifference(message.first.at(0), message.second.at(i));
        findMeasurement->second.disconnectTime.insert(static_cast<int>(measuredTime / 1000));
    }
}

/**
 * @brief finishes the measurements by writing to the CSV file containing the results
 *
 * @param pidApplication the PID of the application (for finding the measurements that must be stored)
 */
void Measurements::finishMeasurement(const unsigned int pidApplication) {
    readMeasurements(pidApplication);
    auto findMeasurement = _measurements.find(pidApplication);
    auto results = findMeasurement->second;
    const double cpuUsage = results.cpuUsage.totalUsage / results.cpuUsage.measurements;
    const double physMemUsage = results.physicalMemoryUsage.totalUsage / results.physicalMemoryUsage.measurements;
    const double virtMemUsage = results.virtualMemoryUsage.totalUsage / results.virtualMemoryUsage.measurements;
    _file->storeCSV(results.connectTime.getLowest(), results.connectTime.getAverage(), results.connectTime.getHighest(),
                    results.disconnectTime.getLowest(), results.disconnectTime.getAverage(),
                    results.disconnectTime.getHighest(), cpuUsage, physMemUsage, virtMemUsage);
    _measurements.erase(pidApplication);
    _debugTopic.reset();
}