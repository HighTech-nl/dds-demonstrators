#include "tests_implementation.hpp"

#include <chrono>
#include <thread>

/**
 * @brief Construct a new TestsImplementation object
 *
 * @param numberOfTests the number of tests that should be executed
 * @param filename the file name where the results are stored
 * @param applicationsLocation the location containing all applications that will be executed
 */
TestsImplementation::TestsImplementation(const int numberOfTests, const std::string &filename,
                                         const std::string &applicationsLocation)
    : _numberOfTests{numberOfTests}, _applicationsLocation{applicationsLocation}, _filename{filename},
      _applicationTopic{&_participant} {
    _participant = dds_create_participant(DDS_DOMAIN_DEFAULT, NULL, NULL);
    _applicationTopic.initialize();
}

/**
 * @brief Destroy the TestsImplementation object
 *
 */
TestsImplementation::~TestsImplementation() { dds_delete(_participant); }

/**
 * @brief execute executes the slave implementation
 *
 * @param matrixboardID the matrix board ID
 * @param unused unused by the slave, function parameters had to correspond to the master application
 */
void TestsImplementation::execute(const unsigned int matrixboardID, const std::string &unused) {
    bool running = true;
    while (running) {
        if (_applicationTopic.receivedNewApplication()) {
            _runApplication.stopMatrixBoard();
            const std::string newApplication{_applicationTopic.getNewApplication()};
            std::cout << "Received application name: " << newApplication << std::endl;
            if (newApplication == "STOP") {
                running = false;
                break;
            }
            Logger::print(Logger::StatusColor::YELLOW, "PERFORMANCE", "Starting new application: ", newApplication);
            executeApplication(matrixboardID, _applicationsLocation + newApplication);
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}

/**
 * @brief executes a matrix board application
 *
 * @param matrixboardID the matrix board ID
 * @param applicationLocation the location of the application
 */
void TestsImplementation::executeApplication(const unsigned int matrixboardID, const std::string &applicationLocation) {
    // Start the matrix board application
    _runApplication.runMatrixBoard(applicationLocation, matrixboardID);
}