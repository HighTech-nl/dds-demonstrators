#ifndef TESTS_IMPLEMENTATION_HPP
#define TESTS_IMPLEMENTATION_HPP

#include <dds/dds.h>
#include <string>

#include "configuration_synchronizer/configuration_synchronizer.hpp"
#include "run_matrixboard/run_application.hpp"

/**
 * @brief This class executes the necessary matrix board application
 *
 */
class TestsImplementation {
  public:
    TestsImplementation(const int numberOfTests, const std::string &filename, const std::string &applicationsLocation);
    ~TestsImplementation();

    void execute(const unsigned int matrixboardID, const std::string &unused = "");

  private:
    const int _numberOfTests;
    const std::string _applicationsLocation;
    const std::string _filename;
    dds_entity_t _participant;

    Performance::ConfigurationSynchronizer _applicationTopic;
    RunApplication _runApplication;
    void executeApplication(const unsigned int matrixboardID, const std::string &applicationLocation);
};

#endif