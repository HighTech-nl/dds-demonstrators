#include "run_application.hpp"

#include <errno.h>    // error handling
#include <signal.h>   // for kill()
#include <string.h>   // error handling strerror
#include <sys/wait.h> // for wait()
#include <unistd.h>   // for fork()

#include "general/logger.hpp"

/**
 * @brief Constructs a new RunApplication object
 *
 * @param enableRead redirects stdout of the application to this class. Then you can read those messages with
 * readFromMatrixboard()
 * @note stdout does not receive the messages of the matrix board application anymore if readFromMatrixboard is enabled!
 */
RunApplication::RunApplication(const bool enableRead) : _pidApplication{0}, _fd{}, _enablePipes{enableRead} {}

/**
 * @brief runs an external matrix board application on a certain location
 *
 * @param applicationLocation the location + name of the application, for example: ../MatrixboardLocation/MatrixBoard
 * @param id the wanted ID of the matrix board
 * @return int the result of starting the matrix board, -1 on an error and 0 on success
 */
int RunApplication::runMatrixBoard(const std::string &applicationLocation, const unsigned int id) {
    char applicationName[100];
    applicationLocation.copy(applicationName, applicationLocation.size());
    char idCstr[11];
    char *functionParameters[] = {applicationName, idCstr, NULL};
    sprintf(idCstr, "%ul", id);
    if (_enablePipes) {
        createPipe();
    }
    if (0 == (_pidApplication = fork())) {
        if (_enablePipes) {
            configurePipesChild();
        }
        if (-1 == execve(applicationLocation.c_str(), functionParameters, NULL)) {
            Logger::print(Logger::LogLevel::ERROR, "Execution Matrixboard failed!: ", applicationName,
                          " probably doesn't exist!");
            return -1;
        }
    } else {
        if (_enablePipes) {
            configurePipesParent();
        }
    }
    return 0;
}

/**
 * @brief creates a pipe
 *
 */
void RunApplication::createPipe() {
    int error = pipe(_fd);
    if (error < 0) {
        Logger::print(Logger::LogLevel::ERROR, "Pipe creation failed!");
    }
}

/**
 * @brief configures the pipes for the child
 * This redirects stdout to the parent (thus this class)
 */
void RunApplication::configurePipesChild() {
    dup2(_fd[1], STDOUT_FILENO);
    close(_fd[0]); // Close read pipe
    close(_fd[1]); // Close write pipe
}

/**
 * @brief configures the pipes for the parent
 * This closes the pipes that the parent does not use
 */
void RunApplication::configurePipesParent() {
    close(_fd[1]); // Close write pipe
}

/**
 * @brief reads the data from the running matrix board application
 *
 * @return std::string the data that was read from the matrix board
 */
std::string RunApplication::readFromMatrixBoard() {
    if (!_enablePipes) {
        return {}; // pipes are not set correctly
    }
    char cString[256];
    int returnVal;
    returnVal = read(_fd[0], cString, 255); // Blocking Read syscall
    if (returnVal < 0) {
        throw std::runtime_error(strerror(errno));
    }
    cString[returnVal] = '\0';
    return cString;
}

/**
 * @brief stops the active matrix board application
 *
 */
void RunApplication::stopMatrixBoard() const {
    if (_pidApplication > 0) {
        kill(_pidApplication, SIGTERM);
        wait(NULL);
    }
}