#ifndef DEBUG_TOPIC_MANAGER_HPP
#define DEBUG_TOPIC_MANAGER_HPP

#include <dds/dds.h>
#include <memory>
#include <string>

#include "MBCData.h"
#include "debug_message_types.hpp"

namespace Debug {

class DebugTopicDDS;

/**
 * @brief a fully static class which MUST be initialized before using.
 * The connect time, register time and disconnect time can be send using the debug topic
 *
 */
class DebugTopicManager {
  public:
    static void initialize(dds_entity_t *participant);

    static void sendConnectTime(const unsigned int topicId);
    static void sendDeviceRegistered(const unsigned int topicId);
    static void sendDeviceRegistered(const unsigned int topicId, const std::string &customTimestamp);
    static void sendDeviceDisconnected(const unsigned int topicId);
    static void sendDeviceDisconnected(const unsigned int topicId, const std::string &customTimestamp);

  private:
    static void sendDebug(const MessageTypes messageType, const unsigned int topicId);
    static void sendDebug(const std::string &customTimestamp, const MessageTypes messageType,
                          const unsigned int topicId);

    static char _buffer[30];
    static std::unique_ptr<DebugTopicDDS> _pDebugTopicDDS;
    static DebugData_DebugInfo _message;
};

} // namespace Debug

#endif