#include "debug_topic_dds.hpp"

namespace Debug {

/**
 * @brief Constructs a new DebugTopicDDS object
 *
 * @param participant the DDS participant that this class is going to use for the DDS communication
 * @param id the ID of the matrix board
 * @param topic the topic of the DebugTopicDDS shared topic
 */
DebugTopicDDS::DebugTopicDDS(dds_entity_t *participant, const std::string &generalTopicName)
    : DDSBasics<DebugData_DebugInfo>{participant, DebugData_DebugInfo_desc}, _topicName{generalTopicName} {}

/**
 * @brief registers to the debug topic
 *
 */
void DebugTopicDDS::initialize(const bool isWriter, const bool isReader) {
    registerToTopic(_topicName, isWriter, isReader);
}

/**
 * @brief configures the qos policies for the own matrixboard object
 *
 * @param qos the qos object
 */
void DebugTopicDDS::configureQoS(dds_qos_t *qos) {
    dds_qset_reliability(qos, DDS_RELIABILITY_RELIABLE, DDS_SECS(10));
    dds_qset_durability(qos, DDS_DURABILITY_TRANSIENT_LOCAL);
    dds_qset_history(qos, DDS_HISTORY_KEEP_LAST, 5); // Keep the last 5 messages
}

} // namespace Debug