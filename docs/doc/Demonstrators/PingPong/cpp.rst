C++ PingPong
------------------

:authors: Sam Laan
:date: March 2020

Description
"""""""""""
This is the documentation for the C++ PingPong solution using the Cyclone DDS library.
It is assumed you have already installed Cyclone DDS.
If you haven't please consult the setup guide of team Hurricane(:ref:`setupCycloneDDS`).



How to run
""""""""""
To run the program navigate to the /src/demonstrators/PingPong/C++ folder.
The source code and CMakeLists of the program are in this folder.
Now to build the solution perform the following commands:

.. code-block:: C++
   
   mkdir build
   cd build
   cmake ..
   make -j4

An executable called "PingPong" should have appeared in the build folder.
It is now possible to run the program using the following command:

.. code-block:: C++
   
   ./PingPong

The program will ask if you want to run as ping or as pong.
Type 1 and press enter for ping, type 2 and press enter for pong.
In ping mode, the program will subscribe to a topic which is one higher than its id.
For example, if you choose id 1 it subscribes to topic 2.
After choosing ping or pong, the program will ask for an id.
This is the previously mentioned one.
Pong should always be started first because it waits for a ping to come in.
An example scenario of when the program will start ping-ponging is as follows:

* Start pong with id 2
* Start ping with id 1

For more information about the program please refer to the source code.
It is heavily commented and should provide all the information you need.