.. _plantuml:

Setup Guide for PlantUML 
------------------------

1) Download the latest ``platnuml.jar`` `here <https://plantuml.com/download>`_.

2) Save the following script as plantuml (without the .sh extension) in some directory, like ~/bin.

.. code-block:: Console

        #!/bin/sh

        PLANTUML=/path/to/plantuml.jar

        if [ -n "${JAVA_HOME}" ] && [ -x "${JAVA_HOME}/bin/java" ] ; then
            JAVA="${JAVA_HOME}/bin/java"

        elif [ -x /usr/bin/java ] ; then
            JAVA=/usr/bin/java

        else
            echo Cannot find JVM
            exit 1

        fi

        $JAVA -jar ${PLANTUML} ${@}    

3) Finally, Add ``~/bin`` to your ``PATH``, with::


      $ export PATH=$PATH:~/bin

.. note::

    Add this last command in your ``.bashrc`` in order to make it persistent. 



