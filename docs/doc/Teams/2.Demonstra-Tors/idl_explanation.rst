.. _idl_explanation:

Interface Description Language (IDL) explanation
------------------------------------------------

:authors: Joost Baars
:date: Feb 2020

The IDL language is used for the general data types between different programming languages. This language is the layer between different programming languages and makes sure that the received message is formatted correctly for the wanted programming language.
In the DDS applications, a message is described using IDL. An example for an IDL file with a message can be seen underneath.

  .. code-block:: Console
  
	module MatrixBoardData
	{
		struct Msg
		{
			long userID; //@key
			long traffic;
			string message;
		};
		#pragma keylist Msg userID
	};

This example creates a module where the wanted data types can be stored in. A struct is used for the message within the module. This message can be send using DDS. 
Three different variables are being stored in this message, two long types and one string type. These are general types which will be converted to a language specific type (string will for example be converted to a char * in C).

The ``#pragma keylist Msg userID`` defines the key of the message. A key groups the message into a certain category. In the DDS matrix board application, the key is the ID of a unique matrix board. This ``#pragma`` method is used for defining a key value within an OMG-DDS type structure.
``long userID`` also has ``//@key`` behind the variable. This is not used in the DDS application but is added for interoperability with the RTI definition. 


Links
"""""
* Information about defining the key within an IDL header: https://stackoverflow.com/questions/31548067/what-does-pragma-keylist-keyword-do