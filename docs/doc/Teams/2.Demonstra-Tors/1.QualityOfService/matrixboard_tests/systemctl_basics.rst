.. _systemctl_basics:

systemctl/systemd basics
------------------------

:authors: Joost Baars
:date: June 2020

Description
"""""""""""
This page describes some information about ``systemctl`` and ``systemd``.

systemctl commands
""""""""""""""""""
``systemctl`` contains some useful commands for managing services (applications 
that can be run). An example of creating a service is shown in 
:ref:`system_configuration_cpp_mb_perf` for creating ``ptpd`` as a service.

Some useful commands are shown below. In these examples, the ``<servicename>`` 
tag should be replaced with a service name. 

The command below requests the status of a service. In this status, the logging 
can be seen from the service. Also, it is shown if a service is running, stopped, 
crashed, or if it is never started in the first place.

.. code-block:: bash

    systemctl status <servicename>

The commands below enable or disable a service. Only one of these commands should 
be used at a time. By enabling a service, the service is started upon boot. By 
disabling a service, the service is not started upon boot.

.. code-block:: bash

    sudo systemctl enable <servicename>
    sudo systemctl disable <servicename>

The commands below start, stop or restart a service. Only one of these commands 
should be used at a time. These commands control a service manually. With the 
start command, a service can manually be started. With the stop command, it can 
be stopped. And as you probably guessed, with the restart command, a service can 
be restarted.

.. code-block:: bash

    sudo systemctl start <servicename>
    sudo systemctl stop <servicename>
    sudo systemctl restart <servicename>

systemd information
"""""""""""""""""""
With various ``systemd`` commands, the boot time and boot order can be shown. 
This can be useful if you want to speed up the boot time on a device or if you 
want to start a service after another service has been initialized (which is used 
in the section above). 

Boot time
~~~~~~~~~
The boot time can be visualized using the following command:

.. code-block:: bash

    systemd-analyze

This command shows the kernel setup time and the service initialization time. 
Each application and their influence of this time can be shown with the 
following command:

.. code-block:: bash

    systemd-analyze blame

Using this command, the influence of every service is shown. 
This way, you can remove or reduce specific applications that take long to 
configure during boot.

Boot order
~~~~~~~~~~
The boot order can also be useful. The order can be adjusted. If you add a 
service manually, you can place it before other applications. This way, you can 
reduce the time it takes to execute that service upon boot. The following command 
stores the boot order in a ``svg`` file (named ``bootup.svg``). 

.. code-block:: bash

    systemd-analyze plot > bootup.svg

If you run on a system without a GUI, this file should be transferred to your 
computer (for example using FTP).

Links
"""""
* `More information about Systemctl`_

.. _More information about Systemctl: https://www.linode.com/docs/quick-answers/linux-essentials/introduction-to-systemctl/