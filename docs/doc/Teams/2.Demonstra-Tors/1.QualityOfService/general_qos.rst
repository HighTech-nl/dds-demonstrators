.. _general_qos:

General findings
----------------

:authors: Joost Baars
:date: Feb 2020

Description
"""""""""""
This page shows the general findings regarding the QoS policies.

Performance
"""""""""""

The ``dds_write`` function is around twice as fast with a static message (a 
message that stays the same) compared to a message that is changing after each 
write. A struct was sent using DDS containing 3 parameters. It did not seem to 
matter how much data was changed in the struct. If the struct was changed (even 
only one byte), the execution time of the write function would be around twice 
as slow. The execution time of one write action was measured in this setup (the 
average of 10k measurements). The average result was around 8500 nanoseconds for 
a static message and around 16000 nanoseconds for a changing message. These 
measurements are not reliable though but it shows that there is a clear difference 
between the two configurations. 

Liveliness of DDS objects
"""""""""""""""""""""""""
The liveliness on a topic can be requested using a function call 
``dds_get_liveliness_changed_status()`` if the liveliness callback QoS is 
configured with ``NULL``. The liveliness can also be configured to call a callback 
function as soon as the liveliness changes. This liveliness callback is called when 
a writer connects to the topic (also if the writer is part of the same participant). 
The liveliness callback is not called for readers. Therefore, the number of active 
readers on a topic cannot be detected.

CycloneDDS missing features
"""""""""""""""""""""""""""
 * The DDS extension for the QoS Database policy
 * Asynchronous publisher does not seem to be implemented (possibly done automatically for you)